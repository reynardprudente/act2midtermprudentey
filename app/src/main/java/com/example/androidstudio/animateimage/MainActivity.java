package com.example.androidstudio.animateimage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {

//    public void animatedImage(View view) {
//        ImageView img1 = (ImageView) findViewById(R.id.img1);
//
////        .translationYBy(700f).setDuration(2000);
////        .alpha(0f).setDuration(2000);
//
//        ImageView img2 = (ImageView) findViewById(R.id.img2);
//
//        if (img1.getAlpha() == 1) {
//            img1.animate().rotationBy(3600).alpha(0f).setDuration(2000);
//            img2.animate().alpha(1f).rotationBy(3600).setDuration(3000);
//
//
//        } else {
//
//            img2.animate().alpha(0f).rotationBy(-3600f).setDuration(3000);
//            img1.animate().rotationBy(-3600f).alpha(1f).setDuration(3000);
//
//
//        }
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView img1 = (ImageView) findViewById(R.id.img1);
        img1.animate().rotationBy(3600).alpha(1f).setDuration(2000);
        Thread timerThread = new Thread() {
            public void run() {
                try {
                    sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Intent intent = new Intent(MainActivity.this,Main2Activity.class);
                    startActivity(intent);
                }
            }
        };
        timerThread.start();
    }
    @Override
       protected  void onPause(){
                super.onPause();
                finish();
            }
    }







